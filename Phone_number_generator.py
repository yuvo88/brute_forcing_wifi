import argparse
import string
import json
import time
argparser = argparse.ArgumentParser()
countries_start = {"israel": {"args_keys": "0123456789", "args_len": "7",
                              "args_beginings": "['054','055','053','052','050','058','08','03']", "args_starting_list": "['0','0','0','0','0','0','0']"}}
try:
    argparser.add_argument('country')
    args = argparser.parse_args()
    args_keys = countries_start[args.country]["args_keys"]
    args_len = countries_start[args.country]["args_len"]
    args_beginings = countries_start[args.country]["args_beginings"]
    args_starting_list = countries_start[args.country]["args_starting_list"]
except:
    argparser.add_argument('len')
    argparser.add_argument('beginings')
    argparser.add_argument('starting_list')
    args = argparser.parse_args()
    args_keys = args.country
    args_len = args.len
    args_beginings = args.beginings
    args_starting_list = args.starting_list

ALLOWED_CHARACTERS = args_keys
NUMBER_OF_CHARACTERS = len(ALLOWED_CHARACTERS)


def characterToIndex(char):
    return ALLOWED_CHARACTERS.index(char)


def indexToCharacter(index):
    if NUMBER_OF_CHARACTERS <= index:
        raise ValueError("Index out of range.")
    else:
        return ALLOWED_CHARACTERS[index]


def next(string):

    if len(string) <= 0:
        string.append(indexToCharacter(0))
    else:
        string[0] = indexToCharacter(
            (characterToIndex(string[0]) + 1) % NUMBER_OF_CHARACTERS)
        if characterToIndex(string[0]) is 0:
            return list(string[0]) + next(string[1:])
    return string


def main():
    sequence = json.loads(args_starting_list.replace("'", '"'))
    length = pow(NUMBER_OF_CHARACTERS, int(args_len))
    starting = args_beginings.replace("'", '"')
    j = 0
    string = ""
    with open('passes.txt', 'a+') as file:
        for begin in json.loads(starting):
            for i in range(0, length):
                sequence = next(sequence)
                string += begin + ''.join(sequence) + "\n"
                j += 1
                if j == 200000:
                        file.write(string)
                        j = 0
                        string = ""
                print(sequence)
            sequence = json.loads(args_starting_list.replace("'", '"'))
        file.write(string)


if __name__ == "__main__":
    main()
